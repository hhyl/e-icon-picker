# 参数配置

名称 | 功能 | 默认值 | 可选值
---|---|---|---
disabled | 是否禁用|false|true/false
readonly | 原生属性，是否只读|false|true/false
clearable | 是否可清空|true|true/false
placement | 弹窗位置|bottom|top/bottom
styles | 组件自定义样式 |空|无
options | 图标参数|{FontAwesome: true, ElementUI: true, addIconList: [], removeIconList: []}|无
options.FontAwesome |是否使用FontAwesome图标|true|true/false
options.ElementUI |是否使用ElementUI图标|true|true/false
options.eIcon |自带彩色图标|true|true/false
options.eIconSymbol |是否使用彩色图标（false：则eIcon图标为单色）|true|true/false
options.addIconList |自定义新增图标列表|[]|无
options.removeIconList |自定义删除图标列表|[]|无
